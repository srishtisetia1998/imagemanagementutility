package com.Nagarro.demo.entity;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="image_details")
public class Image {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="image_id")
	private int Id ;
	
	@Column(name="image_name")
	private String ImageName ;
	
	@Column(name="image_size")
	private int ImageSize ;
	
	@Column(name="image_preview")
	private Blob ImagePreview ;
	
	public Image() {
		
	}
	
	public Image( String imageName, int imageSize, Blob imagePreview) {
		ImageName = imageName;
		ImageSize = imageSize;
		ImagePreview = imagePreview;
	}
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getImageName() {
		return ImageName;
	}
	public void setImageName(String imageName) {
		ImageName = imageName;
	}
	public int getImageSize() {
		return ImageSize;
	}
	public void setImageSize(int imageSize) {
		ImageSize = imageSize;
	}
	public Blob getImagePreview() {
		return ImagePreview;
	}
	public void setImagePreview(Blob imagePreview) {
		ImagePreview = imagePreview;
	}

	@Override
	public String toString() {
		return "Image [Id=" + Id + ", ImageName=" + ImageName + ", ImageSize=" + ImageSize + ", ImagePreview="
				+ ImagePreview + "]";
	}
	
	
	
}
