package com.Nagarro.demo.dao;

import java.util.List;

import com.Nagarro.demo.entity.Image;

public interface ImageDAO {

	public List<Image> getImagesDetails();
	
}
