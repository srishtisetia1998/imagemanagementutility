package com.Nagarro.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.Nagarro.demo.entity.Image;

@Repository
public class ImageDAOHibernateImpl implements ImageDAO{
	
	private EntityManager entityManager;
	
	@Autowired
	public ImageDAOHibernateImpl( EntityManager theEntityManager ) {
		entityManager = theEntityManager ;
	}

	@Override
	public List<Image> getImagesDetails() {
		
		Session currentSession = entityManager.unwrap(Session.class) ;
		
		Query<Image> theQuery = currentSession.createQuery("from Image" , Image.class );
		
		List<Image> images = theQuery.getResultList() ;
	
		return images;
	}

}
