package com.Nagarro.demo.mvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.Nagarro.demo.dao.ImageDAO;
import com.Nagarro.demo.entity.Image;

@Controller
@RequestMapping("images")
public class ImageUtilityController {
	
	@Autowired
	private ImageDAO imageDAO ;
	
	@RequestMapping("/list")
	public String listImages(Model theModel)
	{
		List<Image> theImages = imageDAO.getImagesDetails() ;
		theModel.addAttribute("images" , theImages ) ;
		return "list-images" ;
	}

	
	@RequestMapping("/error")
	public String listImages()
	{
		
		return "error-images" ;
	}

}
